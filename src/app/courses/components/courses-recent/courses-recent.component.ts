import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CoursesService } from '../../services/courses.service';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
import {ProfileService} from '../../../profile/services/profile.service';
import {ErrorService} from '@universis/common';
import {LoadingService} from '@universis/common';
import {template, at} from 'lodash';
import {SendMessageToClassComponent} from '../../../messages/components/send-message-to-class/send-message-to-class.component';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {log} from 'util';




@Component({
  selector: 'app-courses-recent',
  templateUrl: './courses-recent.component.html',
  styleUrls: ['./courses-recent.component.scss']
})
export class CoursesRecentComponent implements OnInit {
  public recentCourses: any;
  public courseCurrentExams: any;
  public instructor: any;
  viewMode = 'tab1';
  public CoursesSameDep: any;
  public CoursesDiffDep: any;
  public loading = true;
  public showMessageForm = false;
  bsModalRef: BsModalRef;



  constructor(private _context: AngularDataContext,
              private translate: TranslateService,
              private profile: ProfileService,
              private  coursesService: CoursesService,
              private loadingService: LoadingService,
              private errorService: ErrorService,
              private _profileService: ProfileService,
              private modalService: BsModalService) { }

  ngOnInit() {
// show loading
    this.loadingService.showLoading();
    Promise.all([
      this.profile.getInstructor(),
      this.coursesService.getRecentCourses(),
      this.coursesService.getCourseExams(),
      this.coursesService.getCourseCurrentExams()
    ]).then( results => {
      // get instructor results[0]
      this.instructor = results[0];
      // get recent courses results[1]
      this.recentCourses = results[1];
      // get class and elearning url for courseClasses
      if (this.instructor && this.instructor.department && this.instructor.department.organization &&
        this.instructor.department.organization.instituteConfiguration) {
        const instituteConfig = this.instructor.department.organization.instituteConfiguration;
        this.recentCourses.map(courseClass => {
          courseClass.classUrl = instituteConfig.courseClassUrlTemplate ?
              template(instituteConfig.courseClassUrlTemplate)(courseClass) : null;
          courseClass.eLearningUrl = instituteConfig.eLearningUrlTemplate ?
              template(instituteConfig.eLearningUrlTemplate)(courseClass) : null;
        });
      }
      // filter by department
      this.CoursesSameDep = this.recentCourses.filter(x => {
        return x.course.department.name === this.instructor.department.name ;
      });
      this.CoursesDiffDep = this.recentCourses.filter(x => {
        return x.course.department.name !== this.instructor.department.name ;
      });
      // get course exams results[2]
      this.courseCurrentExams = results[2].value;
      // add related exams to each course class
      this.recentCourses.forEach((x) => {
        return x.exams = (this.courseCurrentExams || []).filter((y) => {
          return y.classes.findIndex((z) => {
            return z.courseClass === x.id;
          }) >= 0;
        });
      });
      // hide loading
      this.loadingService.hideLoading();
      // set loading flag
      this.loading = false;
    }).catch( err => {
      // hide loading
      this.loadingService.hideLoading();
      return this.errorService.navigateToError(err);
    });
  }
  public openModalWithComponent(courseClass) {
    const initialState = {
      'showMessageForm' : !this.showMessageForm,
      'courseClass' : courseClass,
      'instructor' : this.instructor,
    };
    this.bsModalRef = this.modalService.show(SendMessageToClassComponent, {initialState});
  }

  public openUrl(url) {
    if (url) {
      window.open(url, '_blank');
    }
  }
}
function checkScreenSize() {
  return window.innerWidth < 500; // The screen size you would like to enable the click;
}
