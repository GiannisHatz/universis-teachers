import { ClassesModule } from './classes.module';
import { MostModule } from '@themost/angular';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';
import { TestBed, async } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ClassesModule', () => {
  let classesModule: ClassesModule;

  beforeEach(async(() => {
    return TestBed.configureTestingModule({
      imports: [
        CommonModule,
        TranslateModule.forRoot(),
        HttpClientTestingModule,
        MostModule.forRoot({
          base: '/',
          options: {
            useMediaTypeExtensions: false
          }
        })
      ]
    }).compileComponents().then( () => {
      const translateService = TestBed.get(TranslateService);
      classesModule = new ClassesModule(translateService);
    });
  }));

  it('should create an instance', () => {
    expect(classesModule).toBeTruthy();
  });
});
