import { TestBed } from '@angular/core/testing';

import { StudentsService } from './students.service';
import { MostModule } from '@themost/angular';

describe('StudentsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      MostModule.forRoot({
        base: '/',
        options: {
            useMediaTypeExtensions: false
        }
      })
    ]
  }));

  it('should be created', () => {
    const service: StudentsService = TestBed.get(StudentsService);
    expect(service).toBeTruthy();
  });
});
